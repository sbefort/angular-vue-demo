import angular from 'angular';
import 'ngVue';
import 'ngVue/build/plugins.js';

const ngVueTools = angular.module('ngVueTools', ['ngVue', 'ngVue.plugins']);

export default ngVueTools;
