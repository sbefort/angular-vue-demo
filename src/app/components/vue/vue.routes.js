/* @ngInject */
export default function routes($stateProvider) {

    $stateProvider
        .state('vue', {
            url: '/vue/',
            template: require('./vue.html'),
            controller: 'VueController',
            controllerAs: 'vue'
        })

}