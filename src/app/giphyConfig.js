const giphyConfig = {
    PUBLIC_KEY: 'gGQfuhfgycYX5v4n7JusVRRE74Rww0Kr',
    BASE_URL: '//api.giphy.com/v1/gifs/search?',
    LIMIT: 3,
}

export default giphyConfig;
