import '../styles/app.scss'

import angular from 'angular'
import collapse from 'ui-bootstrap4/src/collapse'
import uirouter from '@uirouter/angularjs'
import routing from './app.config'
import AppController from './app.controller'

import home from './components/home';
import vue from './components/vue';
import grids from './components/grids';
import album from './components/album';

angular
    .module('app', [collapse, uirouter, home, vue, grids, album])
    .config(routing)
    .controller('AppController', AppController)