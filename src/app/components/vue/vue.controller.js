import axios from 'axios';
import giphyConfig from '../../giphyConfig.js';
const { BASE_URL, LIMIT, PUBLIC_KEY } = giphyConfig;

/* @ngInject */
export default class VueController {

  constructor ($scope) {
    $scope.giphy = {};
    $scope.giphy.search = '';
    $scope.giphy.result = [];
    this.$scope = $scope;
  }

  async searchGiphy () {
    try {
      const result = await axios.get(`${BASE_URL}q=${this.$scope.giphy.search}&limit=${LIMIT}&api_key=${PUBLIC_KEY}`);
      this.$scope.giphy.result = result.data.data;
      // console.log(this.$scope.giphy.result);
      this.$scope.$applyAsync();
    } catch (err) {
      alert(err);
    } finally {
      // console.log('Done!');
    }
  }
}
