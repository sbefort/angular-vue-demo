import Vue from 'vue';
import ngVueTools from './ngVueTools.js';
import HelloWorld from '../components/HelloWorld.vue';
import GiphyContainer from '../components/GiphyContainer.vue';

ngVueTools.directive('helloWorld',
    /** @ngInject */
    createVueComponent => createVueComponent(Vue.component('helloWorld', HelloWorld))
);

ngVueTools.directive('giphyContainer',
    /** @ngInject */
    createVueComponent => createVueComponent(Vue.component('giphyContainer', GiphyContainer))
);
