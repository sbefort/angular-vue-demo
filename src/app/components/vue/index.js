import './vue.scss'

import angular from 'angular'
import uirouter from '@uirouter/angularjs'

import routing from './vue.routes'
import VueController from './vue.controller'

import '../../../vue/ngVueBridge/ngVueDirectives';
import ngVueTools from '../../../vue/ngVueBridge/ngVueTools';

export default angular.module('app.vue', [uirouter, ngVueTools.name])
    .config(routing)
    .controller('VueController', VueController)
    .name